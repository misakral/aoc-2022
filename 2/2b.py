# OPONONENT A = ROCK, B = PAPER, C = SCISSORS
# ME        X = ROCK, Y = PAPER, Z = SCISSORS
# VALUE     ROCK = 1, PAPER = 2, SCISSORS = 3

SCORECARD = {"X": 1, "Y": 2, "Z": 3}
POINTS = {"L": 0, "D": 3, "W": 6}
OUTCOMES ={
    "D": [["A", "X"], ["B", "Y"], ["C", "Z"]],
    "W": [["B", "Z"], ["A", "Y"], ["C", "X"]],
    "L": [["B", "X"], ["A", "Z"], ["C", "Y"]]
}
INSTRUCTIONS = {
    "Y": "D",
    "X": "L",
    "Z": "W"
}


def get_desired_reaction(input: list):
    outcome = INSTRUCTIONS[input[1]]
    for i in OUTCOMES[outcome]:
        if input[0] == i[0]:
            return i
    return None

def determine_outcome(input: list):
    for outcome, state in OUTCOMES.items():
        if input in state:
            return POINTS[outcome]
    return None

def determine_points(input: list) -> int:
    return SCORECARD[input[1]]

def get_score_per_round(input: list) -> int:
    play = get_desired_reaction(input)
    return (determine_outcome(play) + determine_points(play))

running_total = 0
with open('2_input.txt', 'r') as fid:
    for row in fid:
        running_total += get_score_per_round(row.split())

print(running_total)



