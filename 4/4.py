import re

with open('4_input.txt', 'r') as fid:
    input = fid.read()

sep_input = re.split(',|\n|-', input)
sep_input2 = [sep_input[i:i+4] for i in range(0, len(sep_input), 4)]
sep_input3 = [[int(a) for a in ml] for ml in sep_input2]
res = [1 for a in sep_input3 if ((a[0] <= a[2] and a[1] >= a[3]) or (a[2] <= a[0] and a[3] >= a[1]))]
res2 = [1 for a in sep_input3 if ((a[0] <= a[2] and a[1] >= a[2]) or (a[2] <= a[0] and a[3] >= a[0]))]
print(sum(res))
print(sum(res2))

