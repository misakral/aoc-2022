from collections import Counter
DESIRED_UNIQUE_STR_LEN = 14



def count_unique_chars(in_str:str):
    if (len(set(in_str)) == len(in_str)):
        return True
    return False

def run_subroutine(datastream: str):
    for i,j in enumerate(datastream):
        if count_unique_chars(input[i:i+DESIRED_UNIQUE_STR_LEN]) == True:
            return i+DESIRED_UNIQUE_STR_LEN

with open('6_input.txt', 'r') as fid:
    input = fid.read()

print(run_subroutine(input))