# ORIGINAL
# with open('1_input.csv', 'r') as fid:
#     sums = []
#     temp_sum = 0
#     for a in fid:
#         if a == '\n':
#             sums.append(temp_sum)
#             temp_sum = 0      
#         else:
#             temp_sum = temp_sum + int(a)
#     sums.sort(reverse=True)
#     print(sums)


# REFAC
with open('1_input.txt', 'r') as fid:
    raw_input = fid.read()
    listed_input = raw_input.split("\n\n") ## inspired by TVotava

elf_batch = [batch.split("\n") for batch in listed_input]
elf_sums = [sum(int(i) for i in batch) for batch in elf_batch]
print(max(elf_sums))
elf_sums.sort(reverse=True)
print(sum(elf_sums[:3]))
