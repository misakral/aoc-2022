# ORIGINAL
with open('input_1a.csv', 'r') as fid:
    max = 0
    temp_sum = 0
    for a in fid:
        if a == '\n':
            if temp_sum > max:
                max = temp_sum
            temp_sum = 0
        else:
            temp_sum = temp_sum + int(a)
    print(max)