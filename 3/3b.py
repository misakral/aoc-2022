def get_common_letter(str1: str, str2: str, str3: str) -> str: #think of something prettier
    common_letter = list(set(str1)&set(str2)&set(str3))
    return ''.join(map(str, common_letter))

def get_score(divided_list: list):
    letter = get_common_letter(divided_list[0], divided_list[1], divided_list[2]) 
    if letter.isupper(): 
        return int(ord(letter)-38)
    elif letter.islower(): 
        return int(ord(letter)-96)
    return None

def get_groups(in_str: str) -> list[list]:
    clean_list = input.split('\n')
    clean_list_len = len(clean_list)
    return [clean_list[i:i+3] for i in range(0, clean_list_len, 3)]




with open('3_input.txt', 'r') as fid:
    input = fid.read()

grouped_list = get_groups(input)

scores = [get_score(a) for a in grouped_list]
print(sum(scores))
        

#TODO: REFACTOR
