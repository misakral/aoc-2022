def divide_list(in_str: str) -> list:
    half_len = round(len(in_str)/2)
    return [in_str[:half_len], in_str[half_len:]]

def get_common_letter(str1: str, str2: str) -> str:
    common_letter = list(set(str1)&set(str2))
    return ''.join(map(str, common_letter))

def get_score(divided_list: list):
    letter = get_common_letter(divided_list[0], divided_list[1]) 
    if letter.isupper(): 
        return int(ord(letter)-38)
    elif letter.islower(): 
        return int(ord(letter)-96)
    return None




with open('3_input.txt', 'r') as fid:
    input = fid.read()

half_list = [divide_list(a) for a in input.split('\n')]

scores = [get_score(a) for a in half_list]
print(sum(scores))

        