import re
from typing import List

with open("5_input.txt", "r") as fid:
    raw_input = fid.read()

print(raw_input.split("\n\n")[0].replace("\n", " "))
def parse_crates(raw_input: str):
    stacks = raw_input.split("\n\n")[0].replace("\n", " ")  # string
    stacks_cln = [stacks[i : i + 4] for i in range(0, len(stacks), 4)]  # list
    print(stacks_cln)
    stacks_split = [
        stacks_cln[i : i + 9] for i in range(0, len(stacks_cln), 9)
    ]  # list of lists
    stacks_replace = [
        [re.sub("[\[\] ]", "", b.replace("    ", "-")) for b in a] for a in stacks_split
    ][:-1]
    transposed_tuples = list(zip(*stacks_replace))
    transposed_lists = [list(a) for a in transposed_tuples]
    return [[a for a in b if a != "-"] for b in transposed_lists]


def parse_moves(raw_input: str):
    moves_raw = raw_input.split("\n\n")[1].split("\n")
    r = re.compile(r"\d+")
    moves_list = [list(map(int, r.findall(move))) for move in moves_raw]
    return moves_list


def make_move(stack: List, move: List, cratemover_9000: bool):
    number_of_crates = move[0]
    src = move[1] - 1
    tgt = move[2] - 1
    moved_crates = stack[src][:number_of_crates]
    if cratemover_9000: #true for part a
        moved_crates.reverse()
    stack[tgt] = moved_crates + stack[tgt]
    del stack[src][:number_of_crates]
    return


def get_result(stack: List):
    return "".join([a[0] for a in stack])


parsed_crates = parse_crates(raw_input)
parsed_moves = parse_moves(raw_input)
for move in parsed_moves:
    make_move(parsed_crates, move, True)
print(get_result(parsed_crates))